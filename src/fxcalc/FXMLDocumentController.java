/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxcalc;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

/**
 *
 * @author aluno
 */
public class FXMLDocumentController implements Initializable {
    
    private String CELSIUS = "Celsius";
    private String FAHRENHEIT = "Fahrenheit";
    private String KELVIN = "Kelvin";
    
    @FXML
    private TextField input;
    @FXML 
    private TextField result;
    
    @FXML
    private Button bt1;
    @FXML
    private Button bt2;
    @FXML
    private Button bt3;
    @FXML
    private Button bt4;
    @FXML
    private Button bt5;
    @FXML
    private Button bt6;
    @FXML
    private Button bt7;
    @FXML
    private Button bt8;
    @FXML
    private Button bt9;
    
    @FXML
    private Button btC;
    @FXML
    private Button btDot;
    
    @FXML
    private ChoiceBox selectDe;
    @FXML
    private ChoiceBox selectPara;
    
    @FXML
    private void onAction0(ActionEvent event) {
        input.setText(input.getText() +  "0");
    }
    @FXML
    private void onAction1(ActionEvent event) {
        input.setText(input.getText() +  "1" );
    }
    @FXML
    private void onAction2(ActionEvent event) {
        input.setText(input.getText() + "2");
    }
    @FXML
    private void onAction3(ActionEvent event) {
        input.setText(input.getText() + "3");
    }
    @FXML
    private void onAction4(ActionEvent event) {
        input.setText(input.getText() + "4");
    }
    @FXML
    private void onAction5(ActionEvent event) {
        input.setText(input.getText() + "5");
    }
    @FXML
    private void onAction6(ActionEvent event) {
        input.setText(input.getText() + "6");
    }
    @FXML
    private void onAction7(ActionEvent event) {
        input.setText(input.getText() + "7");
    }
    @FXML
    private void onAction8(ActionEvent event) {
        input.setText( input.getText() + "8");
    }
    @FXML
    private void onAction9(ActionEvent event) {
        input.setText( input.getText()+ "9" );
    }
    @FXML
    private void onActionC(ActionEvent event) {
        input.setText("");
    }
    @FXML
    private void onActionDot(ActionEvent event) {
        input.setText( "." + input.getText());
    }
    
    @FXML
    private void onCompute(ActionEvent event) {
        double num = Double.parseDouble(input.getText());
        // TRANSFORMAMOS TUDO PARA CELSIUS PRIMEIRO
        if(selectDe.getValue().toString().equals(KELVIN)){
            num = num - 273.15;
        }
        if(selectDe.getValue().toString().equals(FAHRENHEIT)){
            num = (num - 32)*5/9;
        }
        // PARA CONVERTER PRO QUE QUISERMOS DEPOIS
        if(selectPara.getValue().toString().equals(KELVIN)){
            num = num + 273.15;
        }
        if(selectPara.getValue().toString().equals(FAHRENHEIT)){
            num = (num*9/5 + 32);
        }
        result.setText("" + num);
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        input.setText("");
        selectDe.setItems(FXCollections.observableArrayList(CELSIUS, FAHRENHEIT, KELVIN));
        selectPara.setItems(FXCollections.observableArrayList(CELSIUS, FAHRENHEIT, KELVIN));
        
    }    
    
}
